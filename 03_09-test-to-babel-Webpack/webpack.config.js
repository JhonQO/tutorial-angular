var globals = require('./package.json');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var path = require('path');

module.exports = {
  entry: [path.join(__dirname, 'app','index.js'), 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true'],
  // entry: [ globals.config.entryPath + '/index.js', 'webpack/hot/dev-server'],
  resolve: {
    	extensions: ['', '.js', '.jsx' ]
  },
  output: {
    path: globals.config.outputPath,
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel-loader', include: path.join(__dirname,'static','js'), exclude: /node_module/, query: { presets: ["es2015"]} },
      {test: /\.js$/, loader: 'babel-loader', include: path.join(__dirname,'build'), exclude: /node_module/, query: { presets: ["es2015"]} },
      { test: /\.css?$/, loader: 'style!css' },
      { test: /\.html$/, loader: 'html-loader', include: path.join(__dirname,'partials')}
          //include: path.join(__dirname, 'src')
    ]
  },
  // ## Esta seccion  de DevServer solo es valido si es llamdo por: npm webpack-dev-server, para usar con el
  // ## el express, se defice en el mismo expres.

  // devServer: {
  //   // port:globals.config.port,
  //   contentBase: path.join(__dirname, 'build'),
  //   inline: true,
  //   stats:'errors-only'
  // },
  plugins: [
    // OccurenceOrderPlugin is needed for webpack 1.x only
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
]
  // # Algunas informaciones para el ambiente de producction: (webpack.BannerPlugin, HtmlWebpackPlugin)
  // plugins: [
  //      new webpack.BannerPlugin("Copyright QO"),
  //      new HtmlWebpackPlugin({
  //         template: __dirname + "/build/index.html",
  //         hast:true // add serializable numeric PARA PRODuCTION.
  //     }),
  //      new webpack.HotModuleReplacementPlugin(),
  //      new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 20 })
  //  ]


}
