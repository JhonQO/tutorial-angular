

var myApp = angular.module("StoreControllerModule",[]);

myApp.controller("KartListCtrl", function($scope, kartService) {
	$scope.kart = kartService.getKart();

	$scope.buy = function(book) {
		kartService.buy(book);
	}
});

myApp.controller("HeaderCtrl", function($scope) {
	$scope.appDetails = {};
	$scope.appDetails.title = "BooKart";
	$scope.appDetails.tagline = "We have collection of 1 Million books";
});

myApp.controller("BookListCtrl", function($scope, bookService, kartService) {
	$scope.books = bookService.getBooks();

	$scope.addToKart = function(book) {
		kartService.addToKart(book);
	}
});
