
 var myApp = angular.module("myApp", ['ui.router',
									'StoreServiceModule',
									'StoreControllerModule']);
									
myApp.config(function($stateProvider, $urlRouterProvider){	
		$urlRouterProvider.otherwise("/books");
		
		$stateProvider
		.state('StateMenuBooks', {
			url:"/books",
			templateUrl:"partials/book-list.html",
			controller: "BookListCtrl"
			}) 
		.state('StateMenuKart', {
				url:"/kart",
				templateUrl: "partials/kart-list.html",
				controller: "KartListCtrl"
		});	

});
