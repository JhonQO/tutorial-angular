'use strict'

var globals = require('./package.json');
var proxy = require('express-http-proxy');
var express = require('express');



var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config');


const app = express();
var httpserver = 'http://localhost:3001';
var compiler = webpack(config);


app.use(webpackDevMiddleware(compiler, {
    'noInfo': true,
    'publicPath': config.output.publicPath
    // 'inline': true,
    // 'hot': true,
    // 'watch': true
  }));

app.use(webpackHotMiddleware(compiler, {
    'log': console.log, //false
    'path': '/__webpack_hmr',
    //path: config.output.publicPath + '__webpack_hmr'
    'heartbeat': 10 * 1000
}));

app
	.get('/index',(req, res) => {

	  res.end(`
		<h1>Hola desde Express
		</h1>`)
	})
	.get('/',(req, res) => {
		console.log( "URL", req.url);
		res.sendFile(__dirname + '/build/index.html');
	})
    .use('/app', express.static('app'))
  .use('/build', express.static('build'))
	.use('/static', express.static('static'))
	.use('/node_modules',express.static('node_modules'))
	.use('/partials',express.static('partials'))
	.listen(globals.config.port, function (error) {
		if (error) {
			console.error(error);
		} else {
			console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', globals.config.port, globals.config.port);
		}
	});
