'use strict'

var globals = require('./package.json');
var proxy = require('express-http-proxy');
var express = require('express'),
    app = express();

	
var httpserver = 'http://localhost:3001';

	
app
	.get('/index',(req, res) => {

	  res.end(`
		<h1>Hola desde Express
		</h1>`)
	})
	.get('/',(req, res) => {
		console.log( "URL", req.url);
		res.sendFile(__dirname + '/index.html');
	})
	
	.use('/static', express.static('static'))
	.use('/node_modules',express.static('node_modules'))
	.use('/partials',express.static('partials'))
	.use('/carelogic', proxy(httpserver, {
		'forwardPath': function (req, res) {
			console.log('carelogic forward ' + req.url);

			return '/carelogic' + req.url;
		}
	}))
	.listen(globals.config.port, function (error) {
		if (error) {
			console.error(error);
		} else {
			console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', globals.config.port, globals.config.port);
		}
	});


  
  // Saludos :D